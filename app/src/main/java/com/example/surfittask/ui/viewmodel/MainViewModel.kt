package com.example.surfittask.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.surfittask.model.Auto
import com.example.surfittask.network.DataState
import com.example.surfittask.network.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor
    (
    private val repository: Repository,
    savedStateHandle: SavedStateHandle,
    application: Application
): AndroidViewModel(application) {
    private val _autoStateFlow = MutableStateFlow<DataState<List<Auto>>>(DataState.Initial)
    val autoStateFlow = _autoStateFlow.asStateFlow()

    init {
        getData()
    }

    fun getData() = viewModelScope.launch {
        try {
            repository.getAutoList().collect{autolist ->
                if (!autolist.isNullOrEmpty()) {
                    _autoStateFlow.emit(DataState.Success(autolist))
                } else {
                    _autoStateFlow.emit(DataState.Error("No Auto"))
                }
            }
           } catch (exception: Exception){
            _autoStateFlow.emit(DataState.Error(exception.message!!))
           }
    }

    fun refresh() = viewModelScope.launch {
        repository.refreshCounter().collect{
        }
    }

}