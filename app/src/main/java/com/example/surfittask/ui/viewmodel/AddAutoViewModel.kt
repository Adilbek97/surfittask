package com.example.surfittask.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.surfittask.model.Auto
import com.example.surfittask.network.DataState
import com.example.surfittask.network.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Calendar
import java.util.Date
import javax.inject.Inject
import kotlin.random.Random

@HiltViewModel
class AddAutoViewModel @Inject constructor
    (
    private val repository: Repository,
    savedStateHandle: SavedStateHandle,
    application: Application
): AndroidViewModel(application) {

    fun addAuto(auto: Auto) = viewModelScope.launch {
        val time = Calendar.getInstance().time
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm")
        val current = formatter.format(time)
        repository.insertAuto(Auto(name = auto.name, image = auto.image, year = auto.year, capacity = auto.capacity, date = current))
    }


}