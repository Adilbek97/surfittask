package com.example.surfittask.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.surfittask.model.Auto
import com.example.surfittask.model.Counter
import com.example.surfittask.network.DataState
import com.example.surfittask.network.Repository
import com.example.surfittask.ui.activity.MainActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class AutoDetailViewModel @Inject constructor
    (
    private val repository: Repository,
    savedStateHandle: SavedStateHandle,
    application: Application
): AndroidViewModel(application) {
    private val _autoStateFlow = MutableStateFlow<DataState<Auto>>(DataState.Initial)
    val autoStateFlow = _autoStateFlow.asStateFlow()
    private var autoId: Int = -1
    init {
        savedStateHandle.getLiveData<Int>(MainActivity.AUTO_Id).value?.let { autoId = it }
        getCounterSize()
    }

    fun getAuto(autoId:Int) = viewModelScope.launch {
        repository.getAutoById(autoId).collect{
            if (it != null) {
                repository.addCounter()
              _autoStateFlow.emit(DataState.Success(it))
            } else {
            _autoStateFlow.emit(DataState.Error("No Auto with this Id"))
            }
        }
    }

    fun getCounterSize() = viewModelScope.launch {
        repository.getCounter().collect {
            if (it.isNullOrEmpty()){
                getAuto(autoId)
            } else if (!it.isNullOrEmpty() && it.size < 2 || containsIsBought(it)){
                getAuto(autoId)
            } else {
                _autoStateFlow.emit(DataState.Loading)
            }
        }
    }

    private fun containsIsBought(it: List<Counter>): Boolean {
        for (item in it){
            if (item.isBought == 1) return true
        }
        return false
    }

    fun buy() = viewModelScope.launch {
        repository.buy().collect()
    }
}