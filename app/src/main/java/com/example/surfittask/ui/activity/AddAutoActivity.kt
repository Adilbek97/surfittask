package com.example.surfittask.ui.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.surfittask.R
import com.example.surfittask.model.Auto
import com.example.surfittask.ui.theme.SurfItTaskTheme
import com.example.surfittask.ui.viewmodel.AddAutoViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddAutoActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SurfItTaskTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val viewModel: AddAutoViewModel = hiltViewModel()
                    var auto: Auto? = null
                    AddAutoBody(onItemValueChange = {
                        auto = it
                    }, onSaveClick = {
                        if (auto != null){
                            viewModel.addAuto(auto!!)
                            setResult(RESULT_OK)
                            finish()
                        }
                    })
                }
            }
        }
    }

    @Composable
    fun AddAutoBody(
        onItemValueChange: (Auto) -> Unit,
        onSaveClick: (Auto) -> Unit,
        modifier: Modifier = Modifier
    ) {
        Column(
            modifier = modifier.padding(dimensionResource(id = R.dimen.padding_medium)),
            verticalArrangement = Arrangement.spacedBy(dimensionResource(id = R.dimen.padding_large))
        ) {
            var auto = Auto()
            ItemInputForm(
                auto = auto,
                onValueChange = onItemValueChange,
                modifier = Modifier.fillMaxWidth()
            )
            Button(
                onClick = { onSaveClick.invoke(Auto()) },
                enabled = true,
                shape = MaterialTheme.shapes.small,
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(text = stringResource(R.string.save_action))
            }
        }
    }

    @Composable
    fun ItemInputForm(
        auto: Auto = Auto(),
        modifier: Modifier = Modifier,
        onValueChange: (Auto) -> Unit = {},
        enabled: Boolean = true
    ) {
        Column(
            modifier = modifier,
            verticalArrangement = Arrangement.spacedBy(dimensionResource(id = R.dimen.padding_medium))
        ) {
            val autoName = remember{mutableStateOf("")}
            OutlinedTextField(
                value = autoName.value,
                onValueChange = { autoName.value = it
                    auto.name = autoName.value
                    onValueChange(auto) },
                label = { Text("Название автомобиля (марка)") },
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                    unfocusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                    disabledContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                ),
                modifier = Modifier.fillMaxWidth(),
                singleLine = true
            )
            val autoYear = remember{mutableStateOf("")}
            OutlinedTextField(
                value = autoYear.value,
                onValueChange = { autoYear.value = it
                    auto.year = autoYear.value.toInt()
                    onValueChange(auto)},
                label = { Text("Год выпуска") },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                    unfocusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                    disabledContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                ),
                modifier = Modifier.fillMaxWidth(),
                enabled = enabled,
                singleLine = true
            )
            val capacity = remember{mutableStateOf("")}
            OutlinedTextField(
                value = capacity.value,
                onValueChange = { capacity.value = it
                    auto.capacity = capacity.value.toDouble()
                            onValueChange(auto)},
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                label = { Text("Объем двигателя") },
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                    unfocusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                    disabledContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                ),
                modifier = Modifier.fillMaxWidth(),
                enabled = enabled,
                singleLine = true
            )
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview() {
        SurfItTaskTheme {
            ItemInputForm()
        }
    }
}

