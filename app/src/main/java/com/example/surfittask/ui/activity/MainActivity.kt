package com.example.surfittask.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.telecom.Call.Details
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.SmallFloatingActionButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.surfittask.R
import com.example.surfittask.model.Auto
import com.example.surfittask.network.DataState
import com.example.surfittask.ui.theme.SurfItTaskTheme
import com.example.surfittask.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    val getResult =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()) {
            if(it.resultCode == Activity.RESULT_OK){
                viewModel?.getData()
            }
        }
    var viewModel: MainViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SurfItTaskTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    viewModel = hiltViewModel()
                    val autoState = viewModel?.autoStateFlow?.collectAsState()
                    if (autoState?.value is DataState.Success) {
                        myApp((autoState.value as DataState.Success<List<Auto>>).data){auto ->

                                startActivity(
                                    Intent(this@MainActivity, AutoDetailActivity::class.java)
                                        .putExtra(AUTO_Id, auto.id)
                                )
                        }
                    } else if (autoState?.value is DataState.Error) {
                        ErrorMassege(message = (autoState.value as DataState.Error).exception)
                    } else{
                        ErrorMassege(message = "")
                    }
                }
            }
        }
    }

    @Composable
    fun AutoCard(auto: Auto,onClick: (Auto) -> Unit) {
        ElevatedCard(
            elevation = CardDefaults.cardElevation(
                defaultElevation = 6.dp
            ),
            modifier = Modifier
                .padding(horizontal = 8.dp, vertical = 8.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(corner = CornerSize(16.dp))

        ) {
            Row(modifier = Modifier.clickable {
                onClick.invoke(auto)
            }) {
                    AutoImage(auto)
                    Column(
                        modifier = Modifier
                            .padding(16.dp)
                            .fillMaxWidth()
                            .align(Alignment.CenterVertically)
                    ) {
                        Text(text = auto.name, style = typography.titleLarge)
                        Text(text = auto.year.toString(), style = typography.bodyMedium)
                        Text(text = auto.capacity.toString(), style = typography.bodyMedium)
                    }
                }
        }
    }

    @Composable
    fun detailsContent(autoList: List<Auto>,onClick: (Auto) -> Unit) {
        var expanded by remember { mutableStateOf(false) }
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (lazyColumn,refresh, addButton) = createRefs()
            val autos = remember { autoList }
            LazyColumn(
                modifier = Modifier.constrainAs(lazyColumn){
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                },
                contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
            ) {
                items(
                    autos
                ) {
                    AutoCard(it,onClick)
                }
            }
            SmallFloatingActionButton(
                onClick = {
                    viewModel?.refresh()
                          },
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp)
                    .constrainAs(refresh) {
                        bottom.linkTo(addButton.top, margin = 16.dp)
                        end.linkTo(parent.end, margin = 16.dp)
                    },
                containerColor = MaterialTheme.colorScheme.secondaryContainer,
                shape = CircleShape,
                contentColor = MaterialTheme.colorScheme.secondary
            ) {
                Icon(Icons.Filled.Refresh, "Small floating action button.")
            }
            SmallFloatingActionButton(
                onClick = {
                    getResult.launch(Intent(this@MainActivity,AddAutoActivity::class.java))
                          },
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp)
                    .constrainAs(addButton) {
                        bottom.linkTo(parent.bottom, margin = 16.dp)
                        end.linkTo(parent.end, margin = 16.dp)
                    },
                containerColor = MaterialTheme.colorScheme.secondaryContainer,
                shape = CircleShape,
                contentColor = MaterialTheme.colorScheme.secondary
            ) {
                Icon(Icons.Filled.Add, "Small floating action button.")
            }

        }
    }

    @Composable
    fun myApp(autoList: List<Auto>,onClick: (Auto) -> Unit) {
        detailsContent(autoList = autoList, onClick = onClick)
    }

    @Composable
    fun ErrorMassege(message: String) {

        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (text, addButton) = createRefs()
            Text(
                text = "$message!",
                modifier = Modifier.constrainAs(text){
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    bottom.linkTo(parent.bottom)
                    end.linkTo(parent.end)
                },

            )
            SmallFloatingActionButton(
                onClick = {
                    getResult.launch(Intent(this@MainActivity,AddAutoActivity::class.java))
                },
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp)
                    .constrainAs(addButton) {
                        bottom.linkTo(parent.bottom, margin = 16.dp)
                        end.linkTo(parent.end, margin = 16.dp)
                    },
                containerColor = MaterialTheme.colorScheme.secondaryContainer,
                shape = CircleShape,
                contentColor = MaterialTheme.colorScheme.secondary
            ) {
                Icon(Icons.Filled.Add, "Small floating action button.")
            }
        }

    }

    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview() {
        SurfItTaskTheme {
            ErrorMassege("Android")
        }
    }


   @Composable
   private fun AutoImage(auto: Auto) {
       Image(
           painter = painterResource(id = R.drawable.car),
           contentDescription = null,
           contentScale = ContentScale.Crop,
           modifier = Modifier
               .padding(8.dp)
               .size(84.dp)
               .clip(RoundedCornerShape(corner = CornerSize(16.dp)))
       )
    }

    companion object {
        val AUTO_Id: String = "autoId"
    }
}
