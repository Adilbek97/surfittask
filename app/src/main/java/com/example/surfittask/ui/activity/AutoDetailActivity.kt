package com.example.surfittask.ui.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.surfittask.model.Auto
import com.example.surfittask.network.DataState
import com.example.surfittask.ui.theme.SurfItTaskTheme
import com.example.surfittask.ui.viewmodel.AutoDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AutoDetailActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SurfItTaskTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val viewModel: AutoDetailViewModel = hiltViewModel()
                    val autoState = viewModel?.autoStateFlow?.collectAsState()
                    if (autoState?.value is DataState.Success) {
                        ProfileScreen(auto = (autoState.value as DataState.Success<Auto>).data)
                    } else if (autoState?.value is DataState.Error) {
                        ErrorMassege(message = (autoState.value as DataState.Error).exception)
                    } else if (autoState?.value is DataState.Loading){
                        val openAlertDialog = remember { mutableStateOf(true) }
                        if (openAlertDialog.value == true) {
                            AlertDialogExample(
                                onDismissRequest = { openAlertDialog.value = false },
                                onConfirmation = {
                                    viewModel.buy()
                                    openAlertDialog.value = false },
                                dialogTitle = " Купить подписку",
                                dialogText = "Купите подписку. 3000с за 3 месяц.",
                                icon = Icons.Default.Info
                            )
                        } else {
                            onBackPressedDispatcher.onBackPressed()
                        }
                    } else {
                        ErrorMassege(message = "")
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun AlertDialogExample(
        onDismissRequest: () -> Unit,
        onConfirmation: () -> Unit,
        dialogTitle: String,
        dialogText: String,
        icon: ImageVector,
    ) {
        AlertDialog(
            icon = {
                Icon(icon, contentDescription = "Example Icon")
            },
            title = {
                Text(text = dialogTitle)
            },
            text = {
                Text(text = dialogText)
            },
            onDismissRequest = {
                onDismissRequest()
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        onConfirmation()
                    }
                ) {
                    Text("Купить")
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        onDismissRequest()
                    }
                ) {
                    Text("Нет, спасибо")
                }
            }
        )
    }

    @Composable
    fun ErrorMassege(message: String) {

        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (text, addButton) = createRefs()
            Text(
                text = "$message!",
                modifier = Modifier.constrainAs(text){
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    bottom.linkTo(parent.bottom)
                    end.linkTo(parent.end)
                },
                )
        }

    }
}