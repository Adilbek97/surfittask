package com.example.surfittask.di

import android.content.Context
import androidx.room.Room
import com.example.surfittask.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DbModule {
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME
        ).
        fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideAutoDao(db: AppDatabase) = db.autoDao()
    @Singleton
    @Provides
    fun provideCounterDao(db: AppDatabase) = db.counterDao()
}