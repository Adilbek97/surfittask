package com.example.surfittask.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.surfittask.model.Counter
@Dao
interface CounterDao {
    @Query("SELECT * FROM counter")
    fun getAllCounter() : List<Counter>?
    @Query("SELECT * FROM counter WHERE isBought = 1")
    fun getIsBought() : List<Counter>?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(counter: Counter)
    @Query("DELETE FROM counter")
    suspend fun clear()
}