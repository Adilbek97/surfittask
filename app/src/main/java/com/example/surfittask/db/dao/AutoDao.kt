package com.example.surfittask.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.surfittask.model.Auto

@Dao
interface AutoDao {
    @Query("SELECT * FROM auto")
    fun getAllAuto() : List<Auto>?

    @Query("SELECT * FROM auto WHERE id = :id")
    fun getAuto(id: Int): Auto?

    @Query("SELECT * FROM auto WHERE id In (:ids)")
    fun getAutoById(ids: List<Int>): List<Auto>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(autoList: List<Auto>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(auto: Auto)
}