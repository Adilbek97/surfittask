package com.example.surfittask.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.surfittask.db.dao.AutoDao
import com.example.surfittask.db.dao.CounterDao
import com.example.surfittask.model.Auto
import com.example.surfittask.model.Counter

@Database(entities = [Auto::class,Counter::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun autoDao(): AutoDao
    abstract fun counterDao(): CounterDao

    companion object{
        val DATABASE_NAME: String = "Auto-Db"
    }
}