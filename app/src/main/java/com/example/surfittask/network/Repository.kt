package com.example.surfittask.network

import com.example.surfittask.db.dao.AutoDao
import com.example.surfittask.db.dao.CounterDao
import com.example.surfittask.model.Auto
import com.example.surfittask.model.Counter
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ActivityRetainedScoped
class Repository @Inject constructor(
    private val autoDao: AutoDao,
    private val counterDao: CounterDao
) {

    suspend fun getAutoList():Flow<List<Auto>?> =
            flow {
                val autoList = autoDao.getAllAuto()

                emit(autoList)
            }.flowOn(Dispatchers.IO)

    suspend fun insertAuto(auto: Auto){
        autoDao.insert(auto)
    }

    fun getAutoById(autoId: Int): Flow<Auto?> = flow { emit( autoDao.getAuto(autoId)) }.flowOn(Dispatchers.IO)
    suspend fun addCounter() {
        counterDao.insert(Counter())
    }

    fun getCounter():Flow<List<Counter>?> =
        flow {
            val counterList = counterDao.getAllCounter()

            emit(counterList)
        }.flowOn(Dispatchers.IO)

    fun refreshCounter():Flow<Int> =
        flow {
        counterDao.clear()
            emit(1)
        }.flowOn(Dispatchers.IO)

    fun buy():Flow<Boolean> =
        flow {
            counterDao.insert(Counter(isBought = 1))
            emit(true)
        }.flowOn(Dispatchers.IO)
}