package com.example.surfittask.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "counter")
data class Counter(
    @PrimaryKey(autoGenerate = true) val counter:Int = 0,
    val isBought:Int = 0
)