package com.example.surfittask.model

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "auto")
data class Auto(
    @PrimaryKey(autoGenerate = true) val id:Int = 0,
    var name:String = "",
    var image:String = "",
    var year:Int = 0,
    var capacity:Double = 0.0,
    var date:String = "",
)